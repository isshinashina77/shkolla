import { BrowserModule } from '@angular/platform-browser'
import { HttpClientModule } from '@angular/common/http'
import { NgModule } from '@angular/core'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { LocationComponent } from './location/location.component'

import { ApiService } from './_services/api.service'

@NgModule({
    declarations: [AppComponent, LocationComponent],
    imports: [BrowserModule, AppRoutingModule, HttpClientModule],
    providers: [ApiService],
    bootstrap: [AppComponent],
})
export class AppModule {}
