import { Component, OnInit } from '@angular/core'
import { ApiService } from '../app/_services/api.service'

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
})
export class AppComponent {
    public datas: {}
    public search: string = ''

    constructor(private api: ApiService) {
        this.api.get('initial').subscribe((data: any) => {
            console.log('😊 Inital Data', data)
            this.datas = data.location
        })
    }

    onKey(event) {
        if (event.target.value === '') {
            this.api.get('initial').subscribe((data: any) => {
                console.log('😊 Inital Data', data)
                this.datas = data.location
            })
        } else {
            this.api
                .post('search', {
                    search: event.target.value,
                })
                .subscribe((data: any) => {
                    this.datas = data.location
                    this.search = event.target.value
                    console.log('✔ Periferi Data', data)
                })
        }
    }

    onSelect(filter) {
        if (!this.search) {
            this.api.get(`initial/${filter}`).subscribe((data: any) => {
                console.log('😊 Inital Data', data)
                this.datas = data.location
            })
        } else {
            this.api
                .post(`search/${filter}`, {
                    search: this.search,
                })
                .subscribe((data: any) => {
                    this.datas = data.location
                    console.log('✔ Periferi Data', data)
                })
        }
    }
}
