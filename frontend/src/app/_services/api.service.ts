import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable()
export class ApiService {
  constructor(private http: HttpClient) {}

  get(url: string) {
    return this.http.get(
      `${environment.api_protocol}://${environment.api_domain}/${url}`,
      { withCredentials: true }
    );
  }

  post(url: string, data: any) {
    return this.http.post(
      `${environment.api_protocol}://${environment.api_domain}/${url}`,
      data,
      { withCredentials: true }
    );
  }
}
