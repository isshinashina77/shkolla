## Install dependencies

Run `yarn` or `yarn install`

## Watch in dev

Inside backend to watch typscript run `tsc -w` and `cd dist` run `nodemon index.js`

## To just build typescript

Run `tsc` inside the backend folder and typscript will compile

## Add .env in dist
