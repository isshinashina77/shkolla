import * as express from 'express'
import * as Session from 'cookie-session'
import * as bodyParser from 'body-parser'
import * as cookieParser from 'cookie-parser'

let server

/* const Session = session({
    keys: ['secret@tim'],
    name: 'sesid',
    maxAge: 24 * 60 * 60 * 30000,
    cookie: {domain: 'localhost:3000'},
    path: '/'
});
 */

import { Cors } from './_helpers/middleware'

export class Server {
	public express
	private port
	constructor() {
		this.express = express()
		this.mountMiddleware()
	}
	mountRoutes(): void {
		this.express.use(Cors)
		this.express.use('/', require('./routes/index'))
	}
	private mountMiddleware(): void {
		this.express.use(cookieParser())
		//this.express.use(Session)
		this.express.use(bodyParser.json())
		this.express.use(bodyParser.urlencoded({ extended: true }))
		this.express.enable('trust proxy')
	}

	listen(fn: any): void {
		this.port = process.env.PORT || 8000
		server = this.express.listen(this.port, (e) => {
			fn(e, this.port)
		})
	}
	shutdown(): void {
		server.close()
	}
}
