export const Cors = (req, res, next) => {
	res.header('Access-Control-Allow-Credentials', true)
	res.header('Access-Control-Allow-Origin', process.env.FRONTEND_PROTOCOL + '://' + process.env.FRONTEND_DOMAIN.toLowerCase())
	res.header('Access-Control-Allow-Methods', 'OPTIONS,GET,POST')
	res.header('Access-Control-Allow-Headers', 'Origin,Content-Type')

	if ('OPTIONS' === req.method) {
		res.sendStatus(200)
	} else {
		next()
	}
}
