import * as express from 'express'
import { LOADIPHLPAPI } from 'dns'
const axios = require('axios').default
const apiKey = 'AIzaSyBy0MfgWl9IukdYySepyitzk4DwDYONC8M'
export = (() => {
	let router = express.Router()

	/*
    https://maps.googleapis.com/maps/api/place/details/json?place_id=ChIJOUgUMhUBThMRIjt9kgWNiLg&fields=photos,name,rating,formatted_phone_number&key=AIzaSyBy0MfgWl9IukdYySepyitzk4DwDYONC8M
    https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=CmRaAAAAay99yTBzU_1dcgSxPKRYaEmUNKNRAOaisubPelGmarlPM-oghPl7rkuzwAm73L3Xjxmb0cUI3x9Eo-y4oS4ZynkJ8UbR6ofEQaO6WLzzb5LIllMD0lCvdSbcMb3uhYzzEhD51DFN6tqyZxA8WDPfxE49GhRr1VcwPEQx0QJnEr44R5IXtzFM4A&key=AIzaSyBy0MfgWl9IukdYySepyitzk4DwDYONC8M
*/

	router.get('/initial', async (req, res) => {
		const shkoder = await axios.get(
			`https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=42.068087,19.512263&type=point_of_interest&radius=13000&language=sq&key=${apiKey}`
		)

		/*         const details = await axios.get(
            `https://maps.googleapis.com/maps/api/place/details/json?place_id=${shkoder.data.results.place_id}&fields=all&language=sq&key==${apiKey}`
        )
        console.log(details.data) */

		const result = shkoder.data.results.filter((e) => {
			return e.hasOwnProperty('photos')
		})

		let photosArray = await photosFilter(result)
		result.forEach((e, i) => {
			e['photolink'] = photosArray[i]
		})
		return res.json({
			location: result,
		})
	})

	router.get('/initial/:param', async (req, res) => {
		const shkoder = await axios.get(
			`https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=42.068087,19.512263&type=${req.params.param}&language=sq&radius=13000&key=${apiKey}`
		)

		const result = shkoder.data.results.filter((e) => {
			return e.hasOwnProperty('photos')
		})

		let photosArray = await photosFilter(result)
		result.forEach((e, i) => {
			e['photolink'] = photosArray[i]
		})
		return res.json({
			location: result,
		})
	})

	router.post('/search', async (req, res) => {
		//Makes the main search

		const location = await axios.get(
			`https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=${req.body.search}+" Shkoder"&inputtype=textquery&language=sq&fields=all&key=${apiKey}`
		)
		//Uses the search to get the sorrounding places
		const radius = await axios.get(
			`https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=${location.data.candidates[0].geometry.location.lat},${location.data.candidates[0].geometry.location.lng}&type=point_of_interest&language=sq&radius=3500&key=${apiKey}`
		)

		const result = radius.data.results.filter((e) => {
			return e.hasOwnProperty('photos')
		})

		let photosArray = await photosFilter(result)
		result.forEach((e, i) => {
			e['photolink'] = photosArray[i]
		})
		return res.json({
			location: result,
		})
	})

	router.post('/search/:param', async (req, res) => {
		//Makes the main search

		const location = await axios.get(
			`https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=${req.body.search}+" Shkoder"&inputtype=textquery&language=sq&fields=all&key=${apiKey}`
		)
		//Uses the search to get the sorrounding places
		const radius = await axios.get(
			`https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=${location.data.candidates[0].geometry.location.lat},${location.data.candidates[0].geometry.location.lng}&type=${req.params.param}&language=sq&radius=3500&key=${apiKey}`
		)

		const result = radius.data.results.filter((e) => {
			return e.hasOwnProperty('photos')
		})

		let photosArray = await photosFilter(result)
		result.forEach((e, i) => {
			e['photolink'] = photosArray[i]
		})
		return res.json({
			location: result,
		})
	})

	async function photosFilter(result) {
		let photosArray = await Promise.all(
			result.map(async (e) => {
				let photos = await axios.get(
					`https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=${e.photos[0].photo_reference}&language=sq&key=${apiKey}`,
					{
						maxRedirects: 0,
						validateStatus: null,
					}
				)

				let pdata = photos.data.toString()
				let rlength = pdata.length
				let photosstring = `https://${pdata.substr(176, rlength - 205).toString()}`

				return photosstring
			})
		)
		return photosArray
	}

	return router
})()
