require('dotenv').config()

import { Server } from './server'

const server = new Server()

server.mountRoutes()

server.listen((e, port) => {
    if (e) {
        console.error('Could not start server')
        process.exit(e)
    }
    console.log(`Listening on port ${port}`)
})
